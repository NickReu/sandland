module gitlab.com/NickReu/sandland

go 1.13

require (
	github.com/hajimehoshi/ebiten/v2 v2.1.0
	github.com/lucasb-eyer/go-colorful v1.2.0
)

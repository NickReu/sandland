package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"math"
	"math/rand"
	"os"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/lucasb-eyer/go-colorful"
)

const windowSize int = 230
const scaleFactor int = 4
const hrr int = 10

type field [windowSize][windowSize]particle
type heatmap [windowSize / hrr][windowSize / hrr]float64

type heatModel struct{}

// Game implements ebiten.Game interface.
type Game struct {
	grains       *field
	heat         *heatmap
	noise        image.Image
	colorOffsetX int
	colorOffsetY int
	colorWheel   float64
	showHeat     bool
}

type material struct {
	id    int
	name  string
	color color.RGBA
}

type particle struct {
	material material
	age      int
	data     int
	updated  bool
	color    color.RGBA
}

var g *Game

var air material = material{0, "Air", color.RGBA{0, 0, 0, 0}}
var sand material = material{1, "Sand", color.RGBA{255, 226, 145, 255}}
var water material = material{2, "Water", color.RGBA{104, 160, 186, 255}}
var fish material = material{3, "Fish", color.RGBA{186, 66, 35, 255}}
var acorn material = material{4, "Acorn", color.RGBA{105, 100, 60, 255}}
var wood material = material{5, "Wood", color.RGBA{107, 89, 73, 255}}
var leaf material = material{6, "Leaf", color.RGBA{88, 110, 83, 255}}
var fire material = material{7, "Fire", color.RGBA{250, 160, 100, 255}}
var ash material = material{8, "Ash", color.RGBA{128, 120, 113, 255}}
var smoke material = material{9, "Smoke", color.RGBA{196, 194, 192, 255}}
var char material = material{10, "Char", color.RGBA{190, 100, 39, 255}}
var dirt material = material{11, "Dirt", color.RGBA{74, 52, 30, 255}}
var grass material = material{12, "Grass", color.RGBA{134, 201, 117, 255}}
var source material = material{13, "Source", color.RGBA{100, 100, 250, 255}}
var sink material = material{14, "Sink", color.RGBA{250, 100, 100, 255}}
var fairyDust material = material{15, "Fairy Dust", color.RGBA{0, 0, 0, 0}}
var magic material = material{16, "Magic", hslToRGBA(0, 0.8, 0.9)}
var rock material = material{17, "Rock", color.RGBA{100, 100, 100, 255}}
var lava material = material{18, "Lava", color.RGBA{217, 94, 0, 255}}
var steam material = material{19, "Steam", color.RGBA{210, 225, 230, 0}}
var ice material = material{20, "Ice", color.RGBA{200, 225, 250, 255}}

var fluids = []material{air, smoke, steam, water, lava, magic}
var gasses = []material{air, smoke, steam}
var immaterial = []material{fire, magic}
var flammable = []material{wood, leaf, grass}

var getMatByID map[int]material = map[int]material{0: air, 1: sand, 2: water,
	3: fish, 4: acorn, 5: wood, 6: leaf, 7: fire, 8: ash, 9: smoke, 10: char,
	11: dirt, 12: grass, 13: source, 14: sink, 15: fairyDust, 16: magic,
	17: rock, 18: lava, 19: steam, 20: ice,
}

func newParticle(m material) (p particle) {
	p.material = m
	if m.id != fairyDust.id {
		p.color = varyColor(m.color)
	} else {
		p.color = color.RGBA{255, 255, 255, 255}
	}
	return p
}

func hslToRGBA(h float64, s float64, l float64) color.RGBA {
	clr := colorful.HSLuv(float64(h), float64(s), float64(l))
	return color.RGBA{uint8(clr.R * 255), uint8(clr.G * 255), uint8(clr.B * 255), 255}
}

func imageToHeatmap(img image.Image) heatmap {
	b := img.Bounds()
	x0 := b.Min.X
	x1 := b.Max.X
	y0 := b.Min.Y
	y1 := b.Max.Y
	var h heatmap
	for i := 0; i < x1; i++ {
		for j := 0; j < y1; j++ {
			h[i][j] = h.ColorModel().Convert(img.At(x0+i, y0+j)).(colorful.Color).R
		}
	}
	return heatmap{}
}

func colorfulize(c color.Color) colorful.Color {
	r, g, b, _ := c.RGBA()
	return colorful.Color{R: float64(r) / 0xffff, G: float64(g) / 0xffff, B: float64(b) / 0xffff}
}

func (f *field) moveGrain(ox int, oy int, dx int, dy int, mats []material) bool {
	allowed := map[int]bool{}
	for _, m := range mats {
		allowed[m.id] = true
	}
	if ox < 0 || ox >= windowSize || oy < 0 || oy >= windowSize || dx < 0 || dx >= windowSize || dy < 0 || dy >= windowSize || (f[dx][dy].material.id != sink.id && !allowed[f[dx][dy].material.id]) {
		return false
	} else if f[dx][dy].material.id == sink.id {
		f[ox][oy] = newParticle(air)
		f[ox][oy].updated = true
		return true
	}
	temp := f[dx][dy]
	f[dx][dy] = f[ox][oy]
	f[ox][oy] = temp
	f[ox][oy].updated = true
	return true
}

func (f *field) copyGrain(ox int, oy int, dx int, dy int, mats []material) bool {
	allowed := map[int]bool{}
	for _, m := range mats {
		allowed[m.id] = true
	}
	if ox < 0 || ox >= windowSize || oy < 0 || oy >= windowSize || dx < 0 || dx >= windowSize || dy < 0 || dy >= windowSize || !allowed[f[dx][dy].material.id] {
		return false
	}
	f[dx][dy] = f[ox][oy]
	f[dx][dy].updated = true
	return true
}

func (f *field) spawnGrain(dx int, dy int, mat material, mats []material) bool {
	allowed := map[int]bool{}
	for _, m := range mats {
		allowed[m.id] = true
	}
	if dx < 0 || dx >= windowSize || dy < 0 || dy >= windowSize || !allowed[f[dx][dy].material.id] {
		return false
	}
	f[dx][dy] = newParticle(mat)
	return true
}

func (f field) nhLessThan(x, y int, m material, l int) bool {
	count := 0
	for i := -1; i <= 1; i++ {
		if x+i < 0 || x+i >= windowSize {
			continue
		}
		for j := -1; j <= 1; j++ {
			if y+j < 0 || y+j >= windowSize {
				continue
			}
			if f[x+i][y+j].material.id == m.id {
				count++
				if count >= l {
					return false
				}
			}
		}
	}
	return true
}

func (f field) nh4LessThan(x, y int, m material, l int) bool {
	var i, j, count int
	i = -1
	if y >= 0 && y < windowSize {
		if x+i >= 0 && x+i < windowSize && f[x+i][y].material.id == m.id {
			if count >= l {
				return false
			}
		}
		i = 1
		if x+i >= 0 && x+i < windowSize && f[x+i][y].material.id == m.id {
			if count >= l {
				return false
			}
		}
	}
	j = -1
	if x >= 0 && x < windowSize {
		if y+j >= 0 && y+j < windowSize && f[x][y+j].material.id == m.id {
			if count >= l {
				return false
			}
		}
		j = 1
		if y+j >= 0 && y+j < windowSize && f[x][y+j].material.id == m.id {
			if count >= l {
				return false
			}
		}
	}
	return true
}

func (f field) nhAtLeast(x, y int, m material, l int) bool {
	count := 0
	for i := -1; i <= 1; i++ {
		if x+i < 0 || x+i >= windowSize {
			continue
		}
		for j := -1; j <= 1; j++ {
			if y+j < 0 || y+j >= windowSize {
				continue
			}
			if f[x+i][y+j].material.id == m.id {
				count++
				if count >= l {
					return true
				}
			}
		}
	}
	return false
}

func (f *field) ignite(x, y int) {
	for i := -2; i <= 2; i++ {
		if x+i < 0 || x+i >= windowSize {
			continue
		}
		for j := -2; j <= 2; j++ {
			if y+j < 0 || y+j >= windowSize {
				continue
			}
			if rand.Float32() < 0.01 {
				if rand.Float32() < 0.8 {
					f.spawnGrain(x+i, y+j, char, flammable)
				} else {
					f.spawnGrain(x+i, y+j, fire, flammable)
				}
			} else if f[x+i][y+j].material == water {
				f[x][y] = newParticle(smoke)
			}
		}
	}
}

func (f *field) updateSand(x, y int) {

	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 1 {
		return
	}
	if !f.moveGrain(x, y, x, y+1, fluids) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		f.moveGrain(x, y, x+direction, y+1, fluids)
	}
}

func (f *field) updateWater(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 2 {
		return
	}
	if rand.Float32() < 0.2 {
		f[x][y].color = varyColor(f[x][y].material.color)
	}
	if rand.Float32() < 0.01 {
		if g.heat.checkHeat(x, y) > 0.7 {
			g.heat.setHeat(x, y, 0.5)
			f[x][y] = newParticle(steam)
			return
		} else if g.heat.checkHeat(x, y) < 0.3 {
			g.heat.addHeat(x, y, 0.02)
			f[x][y] = newParticle(ice)
			return
		}
	}
	if !f.moveGrain(x, y, x, y+1, append(gasses, ice)) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		if !f.moveGrain(x, y, x+direction, y+1, append(gasses, ice)) {
			for direction < 3 && direction > -3 {
				if direction < 0 {
					direction--
				} else {
					direction++
				}
				if f.moveGrain(x, y, x+direction, y, append(gasses, ice)) {
					break
				}
			}
		}
	}
	if rand.Float32() < 0.005 {
		for i := -2; i <= 2; i++ {
			if x+i < 0 || x+i >= windowSize {
				continue
			}
			for j := -2; j <= 2; j++ {
				if y+j < 0 || y+j+2 >= windowSize {
					continue
				}
				if f[x+i][y+j+1].material == dirt || f[x+i][y+j+1].material == grass && f[x+i][y+j+2].material == dirt {
					f.spawnGrain(x+i, y+j, grass, []material{air})
				}
			}
		}
	}
}

func (f *field) updateFish(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 3 {
		return
	}
	if f[x][y].age == 1 {
		f[x][y].color = color.RGBA{uint8(150*rand.Intn(2) + 100), uint8(150*rand.Intn(2) + 10), uint8(100*rand.Intn(2) + 10), 255}
	}
	if !f.moveGrain(x, y, x, y+1, gasses) {
		if rand.Float32() < 0.2 {
			var axis, direction int
			if rand.Float32() < 0.5 {
				direction = 1
			} else {
				direction = -1
			}
			if rand.Float32() < 0.5 {
				axis = 1
			} else {
				axis = -1
			}
			if axis == 1 {
				f.moveGrain(x, y, x+direction, y, []material{water})
			} else {
				f.moveGrain(x, y, x, y+direction, []material{water})
			}
		}
	}
}

func (f *field) updateAcorn(x, y int) {

	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 4 {
		return
	}
	if !f.moveGrain(x, y, x, y+1, fluids) {
		if f[x][y].age > 350 {
			f[x][y] = newParticle(wood)
		}
	}
}

func (f *field) updateWood(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 5 {
		return
	}
	if f[x][y].age < 80 {
		if rand.Float32() < 0.4 || (x+1 < windowSize && x-1 >= 0 && rand.Float32() < 0.8 && f[x+1][y].material.id == 5 && f[x-1][y].material.id == 5) {
			f.copyGrain(x, y, x, y-1, []material{air, smoke, steam, water, sand, ash, dirt, grass, leaf})
		}
		if f[x][y].age < 15 && rand.Float32() < 0.05 {
			f.copyGrain(x, y, x+1, y, []material{air, smoke, steam, water, sand, ash, dirt, leaf})
		}
		if f[x][y].age < 15 && rand.Float32() < 0.05 {
			f.copyGrain(x, y, x-1, y, []material{air, smoke, steam, water, sand, ash, dirt, leaf})
		}
		if f[x][y].age < 20 && rand.Float32() < 0.9 {
			if f.copyGrain(x, y, x, y+1, []material{air, smoke, steam, water, sand, ash, dirt, grass, leaf}) {
				f[x][y+1].age -= 1
			}
		}
	}
	if f[x][y].age >= 80 && f[x][y].age < 100 {
		if y-1 > 0 && f[x][y-1].material.id == 0 {
			if rand.Float32() < 0.2 {
				var axis, direction int
				if rand.Float32() < 0.5 {
					direction = 1
				} else {
					direction = -1
				}
				if rand.Float32() < 0.5 {
					axis = 1
				} else {
					axis = -1
				}
				if axis == 1 {
					f.spawnGrain(x+direction, y, leaf, gasses)
				} else {
					f.spawnGrain(x, y+direction, leaf, gasses)
				}
			}
		}
	}
}

func (f *field) updateLeaf(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 6 {
		return
	}
	if f[x][y].age < 5500 && rand.Float32() < 0.01 {
		if !f.nhLessThan(x, y, leaf, 6) {
			f[x][y].age += 10000
			return
		}
		var axis, direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		if rand.Float32() < 0.5 {
			axis = 1
		} else {
			axis = -1
		}
		if axis == 1 {
			if f.nhLessThan(x+direction, y, leaf, 5) {
				f.copyGrain(x, y, x+direction, y, gasses)
			} else {
				f[x][y].age += 1000
			}
		} else {
			if f.nhLessThan(x, y+direction, leaf, 5) {
				f.copyGrain(x, y, x, y+direction, gasses)
			} else {
				f[x][y].age += 1000
			}
		}
	}
}

func (f *field) updateFire(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 7 {
		return
	}
	g.heat.addHeat(x, y, 0.0002)
	if f[x][y].color.B > 0 {
		f[x][y].color.B -= 1
	}
	if f[x][y].color.G > 0 {
		f[x][y].color.G -= 1
	}
	if (f[x][y].age > 5 && rand.Float32() < 0.1) || f[x][y].age > 50 {
		if rand.Float32() < 0.5 {
			f[x][y] = newParticle(smoke)
		} else {
			f[x][y] = newParticle(air)
		}
		return
	}
	f.ignite(x, y)
	if rand.Float32() < 0.1 {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		f.moveGrain(x, y, x+direction, y-1, gasses)
	} else if !f.moveGrain(x, y, x, y-1, gasses) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		f.moveGrain(x, y, x+direction, y-1, gasses)
	}
}

func (f *field) updateAsh(x, y int) {
	if f[x][y].age > 2000 && rand.Float32() < 0.008 {
		f[x][y] = newParticle(dirt)
		return
	}
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 8 {
		return
	}
	if !f.moveGrain(x, y, x, y+1, fluids) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		f.moveGrain(x, y, x+direction, y+1, fluids)
	}
}

func (f *field) updateSmoke(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 9 {
		return
	}
	if f[x][y].age > 200 && rand.Float32() < 0.002 {
		if rand.Float32() < 0.01 {
			f[x][y] = newParticle(ash)
		} else {
			f[x][y] = newParticle(air)
		}
		return
	}
	if !f.moveGrain(x, y, x, y-1, []material{air}) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		if !f.moveGrain(x, y, x+direction, y-1, []material{air}) {
			for direction < 5 && direction > -5 {
				if direction < 0 {
					direction--
				} else {
					direction++
				}
				if f.moveGrain(x, y, x+direction, y, []material{air}) {
					break
				}
			}
		}
	}
}
func (f *field) updateSteam(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != steam.id {
		return
	}
	if f[x][y].age > 200 && rand.Float32() < 0.002 && g.heat.checkHeat(x, y) < 0.6 {
		if rand.Float32() < 0.05 {
			f[x][y] = newParticle(water)
		} else {
			f[x][y] = newParticle(air)
		}
		return
	}
	if !f.moveGrain(x, y, x, y-1, []material{air, smoke}) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		if !f.moveGrain(x, y, x+direction, y-1, []material{air, smoke}) {
			for direction < 5 && direction > -5 {
				if direction < 0 {
					direction--
				} else {
					direction++
				}
				if f.moveGrain(x, y, x+direction, y, []material{air, smoke}) {
					break
				}
			}
		}
	}
}

func (f *field) updateChar(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 10 {
		return
	}
	g.heat.addHeat(x, y, 0.0001)
	if rand.Float32() < 0.4 {
		f.spawnGrain(x, y-1, fire, []material{air})
	}
	if f[x][y].age > 30 {
		if !f.moveGrain(x, y, x, y+1, fluids) {
			var direction int
			if rand.Float32() < 0.5 {
				direction = 1
			} else {
				direction = -1
			}
			f.moveGrain(x, y, x+direction, y+1, fluids)
		}
	}
	if f[x][y].age > 20 && rand.Float32() < 0.01 {
		if rand.Float32() < 0.7 {
			f[x][y] = newParticle(fire)
		} else {
			f[x][y] = newParticle(ash)
		}
		return
	}
	f.ignite(x, y)
}

func (f *field) updateDirt(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 11 {
		return
	}
	if !f.moveGrain(x, y, x, y+1, fluids) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		f.moveGrain(x, y, x+direction, y+1, fluids)
	}
}

func (f *field) updateGrass(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 12 {
		return
	}
	if !f.moveGrain(x, y, x, y+1, fluids) {
		for i := -2; i <= 2; i++ {
			if x+i < 0 || x+i >= windowSize {
				continue
			}
			for j := -2; j <= 2; j++ {
				if y+j < 0 || y+j+2 >= windowSize {
					continue
				}
				if rand.Float32() < 0.05 {
					if f[x+i][y+j+1].material == dirt || f[x+i][y+j+1].material == grass && f[x+i][y+j+2].material == dirt {
						f.spawnGrain(x+i, y+j, grass, []material{air})
					}
				}
			}
		}
		if y+2 < windowSize && f[x][y+1].material == grass && f[x][y+2].material == grass {
			f[x][y] = newParticle(air)
		} else if y-1 >= 0 && f[x][y-1].material.id != 0 && f[x][y-1].material != grass && rand.Float32() < 0.1 {
			f[x][y] = newParticle(dirt)
		}
	}
}

func (f *field) updateSource(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 13 {
		return
	}
	if f[x][y].data == -1 {
		return
	}
	if f[x][y].data > 0 {
		var axis, direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		if rand.Float32() < 0.5 {
			axis = 1
		} else {
			axis = -1
		}
		if axis == 1 {
			f.spawnGrain(x+direction, y, getMatByID[f[x][y].data], []material{air})
		} else {
			f.spawnGrain(x, y+direction, getMatByID[f[x][y].data], []material{air})
		}
	} else {
		for i := -1; i <= 1; i++ {
			if x+i < 0 || x+i >= windowSize {
				continue
			}
			for j := -1; j <= 1; j++ {
				if y+j < 0 || y+j+2 >= windowSize || (i == 0 && j == 0) {
					continue
				}
				if f[x+i][y+j].material.id != air.id && f[x+i][y+j].material.id != source.id {
					f[x][y].data = f[x+i][y+j].material.id
					return
				} else if f[x+i][y+j].material.id == source.id {
					f[x][y].data = f[x+i][y+j].data
					return
				}
			}
		}
	}
}

func (f *field) updateFairyDust(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 15 {
		return
	}
	if f[x][y].age%10 == 1 {
		f[x][y].color = hslToRGBA(float64((f[x][y].age/2)%360), 0.8, 0.9)
	}
	if rand.Float32() < 0.001 {
		if f.spawnGrain(x, y-1, magic, []material{air}) {
			f[x][y-1].color = f[x][y].color
		}
	}
	if rand.Float32() < 0.5 && !f.moveGrain(x, y, x, y+1, fluids) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		f.moveGrain(x, y, x+direction, y+1, fluids)
	}
}

func (f *field) updateMagic(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != 16 {
		return
	}
	if f[x][y].color.B > 0 {
		f[x][y].color.B -= 1
	}
	if f[x][y].color.G > 0 {
		f[x][y].color.G -= 1
	}
	if (f[x][y].age > 5 && rand.Float32() < 0.05) || f[x][y].age > 60 {
		f[x][y] = newParticle(air)
		return
	}
	if rand.Float32() < 0.1 {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		f.moveGrain(x, y, x+direction, y-1, gasses)
	} else if rand.Float32() < 0.2 && !f.moveGrain(x, y, x, y-1, gasses) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		f.moveGrain(x, y, x+direction, y-1, gasses)
	}
}

func (f *field) updateLava(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != lava.id {
		return
	}
	g.heat.addHeat(x, y, 0.00005)
	if rand.Float32() < 0.2 {
		f[x][y].color = varyColor(f[x][y].material.color)
	}
	if rand.Float32() < 0.01 {
		if g.heat.checkHeat(x, y) < 0.6 {
			f[x][y] = newParticle(rock)
			return
		}
	}
	if rand.Float64() < 0.5 && !f.moveGrain(x, y, x, y+1, append(gasses, water)) {
		var direction int
		if rand.Float32() < 0.5 {
			direction = 1
		} else {
			direction = -1
		}
		if !f.moveGrain(x, y, x+direction, y+1, append(gasses, water)) {
			for direction < 3 && direction > -3 {
				if direction < 0 {
					direction--
				} else {
					direction++
				}
				if f.moveGrain(x, y, x+direction, y, append(gasses, water)) {
					break
				}
			}
		}
	}
}

func (f *field) updateIce(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != ice.id {
		return
	}
	g.heat.addHeat(x, y, -0.00001)
	if rand.Float32() < 0.01 && g.heat.checkHeat(x, y) > 0.51 {
		f[x][y] = newParticle(water)
		return
	}
	f.moveGrain(x, y, x, y+1, gasses)
}

func (f *field) updateRock(x, y int) {
	if x < 0 || x >= windowSize || y < 0 || y >= windowSize || f[x][y].material.id != rock.id {
		return
	}
	if rand.Float32() < 0.01 && g.heat.checkHeat(x, y) > 0.8 {
		f[x][y] = newParticle(lava)
		return
	}
}

func updateParticle(x, y int) {
	g.grains[x][y].age++
	if !g.grains[x][y].updated {
		g.grains[x][y].updated = true
		switch g.grains[x][y].material.id {
		case 0:
			return
		case sand.id:
			g.grains.updateSand(x, y)
		case water.id:
			g.grains.updateWater(x, y)
		case fish.id:
			g.grains.updateFish(x, y)
		case acorn.id:
			g.grains.updateAcorn(x, y)
		case wood.id:
			g.grains.updateWood(x, y)
		case leaf.id:
			g.grains.updateLeaf(x, y)
		case fire.id:
			g.grains.updateFire(x, y)
		case ash.id:
			g.grains.updateAsh(x, y)
		case smoke.id:
			g.grains.updateSmoke(x, y)
		case char.id:
			g.grains.updateChar(x, y)
		case dirt.id:
			g.grains.updateDirt(x, y)
		case grass.id:
			g.grains.updateGrass(x, y)
		case source.id:
			g.grains.updateSource(x, y)
		case fairyDust.id:
			g.grains.updateFairyDust(x, y)
		case magic.id:
			g.grains.updateMagic(x, y)
		case lava.id:
			g.grains.updateLava(x, y)
		case steam.id:
			g.grains.updateSteam(x, y)
		case ice.id:
			g.grains.updateIce(x, y)
		case rock.id:
			g.grains.updateRock(x, y)
		}
	}
}

func (f *field) paint(tx, ty int, d float64, m material, clear bool) {
	for x, row := range f {
		for y := range row {
			if math.Sqrt(float64((x-tx)*(x-tx)+(y-ty)*(y-ty))) < d {
				if clear || f[x][y].material.id != m.id {
					f[x][y] = newParticle(m)
				}
			}
		}
	}
}

func (h heatmap) At(x, y int) color.Color {
	return colorful.Color{R: h[x][y], G: h[x][y], B: h[x][y]}
}

func (h heatmap) Bounds() image.Rectangle {
	return image.Rectangle{image.Point{0, 0}, image.Point{windowSize / hrr, windowSize / hrr}}
}

func (h heatmap) ColorModel() color.Model {
	return heatModel{}
}

func (m heatModel) Convert(c color.Color) color.Color {
	r, g, b, _ := c.RGBA()
	lum := float64(r)/0xffff*0.299 + float64(g)/0xffff*0.587 + float64(b)/0xffff*0.114
	return colorful.Color{R: lum, G: lum, B: lum}
}

func (h *heatmap) entropy() {
	for x, row := range h {
		for y := range row {
			// h[x][y] = (h[x][y]*90 + 0.5) / 91
			f := (h[x][y] - 0.5) * 0.01
			h[x][y] -= f * 1.1
			spread := false
			for !spread {
				var axis, direction, dx, dy int
				if rand.Float32() < 0.5 {
					direction = 1
				} else {
					direction = -1
				}
				if rand.Float32() < 0.5 {
					axis = 1
				} else {
					axis = -1
				}
				if axis == 1 {
					dx = x
					dy = y + direction
				} else {
					dx = x + direction
					dy = y
				}
				if dx >= 0 && dx < windowSize/hrr && dy >= 0 && dy < windowSize/hrr {
					h[dx][dy] += f
					spread = true
				}
			}
		}
	}
	for x, row := range h {
		for y := range row {
			if h[x][y] > 1 {
				h[x][y] = 1
			} else if h[x][y] < 0 {
				h[x][y] = 0
			}
		}
	}
}

func (h *heatmap) reset() {
	for x, row := range h {
		for y := range row {
			h[x][y] = 0.5
		}
	}
}

func (h *heatmap) addHeat(x, y int, amount float64) {
	if x/hrr >= 0 && x/hrr < windowSize/hrr && y/hrr >= 0 && y/hrr < windowSize/hrr {
		h[x/hrr][y/hrr] += amount
	}
}

func (h *heatmap) setHeat(x, y int, amount float64) {
	if x/hrr >= 0 && x/hrr < windowSize/hrr && y/hrr >= 0 && y/hrr < windowSize/hrr {
		h[x/hrr][y/hrr] = amount
	}
}

func (h heatmap) checkHeat(x, y int) float64 {
	toAdd := []float64{h[x/hrr][y/hrr]}
	var sum float64
	if x/hrr-1 >= 0 && x%hrr < hrr/4 {
		toAdd = append(toAdd, h[x/hrr-1][y/hrr])
	} else if x/hrr+1 < windowSize/hrr && x%hrr > 3*hrr/4 {
		toAdd = append(toAdd, h[x/hrr+1][y/hrr])
	}
	if y/hrr-1 >= 0 && y%hrr < hrr/4 {
		toAdd = append(toAdd, h[x/hrr][y/hrr-1])
	} else if y/hrr+1 < windowSize/hrr && y%hrr > 3*hrr/4 {
		toAdd = append(toAdd, h[x/hrr][y/hrr+1])
	}
	for _, v := range toAdd {
		sum += v
	}
	return sum / float64(len(toAdd))
}

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).
func (g *Game) Update() error {
	// Write your game's logical update.
	if rand.Float32() < 0.5 {
		for x, row := range g.grains {
			for y := range row {
				updateParticle(x, y)
			}
		}

	} else {
		for x, row := range g.grains {
			for y := range row {
				updateParticle(len(g.grains)-1-x, y)
			}
		}
	}
	g.heat.entropy()
	for x, row := range g.grains {
		for y, p := range row {
			if p.updated {
				g.grains[x][y].updated = false
			}
		}
	}
	if ebiten.IsKeyPressed(ebiten.KeyDigit0) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, air, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit1) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, sand, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit2) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, water, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit3) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 1.2, fish, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit4) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 1.2, acorn, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit5) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 2.5, fire, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit6) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, ash, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit7) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 5, rock, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit8) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, dirt, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyDigit9) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 1.2, grass, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyMinus) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 1.2, sink, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyEqual) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 1.2, source, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyGraveAccent) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, fairyDust, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyS) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, steam, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyI) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, ice, false)
	} else if ebiten.IsKeyPressed(ebiten.KeyL) {
		mx, my := ebiten.CursorPosition()
		g.grains.paint(mx, my, 10, lava, false)
		g.heat.setHeat(mx, my, 0.8)
	} else if ebiten.IsKeyPressed(ebiten.KeyC) {
		for x, row := range g.grains {
			for y := range row {
				g.grains[x][y] = newParticle(air)
			}
		}
		g.heat.reset()
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyH) {
		g.showHeat = !g.showHeat
	}
	return nil
}

func Noise(x, y int) float64 {
	r, gr, b, _ := g.noise.At(x, y).RGBA()
	lum := 0.299*float64(r) + 0.587*float64(gr) + 0.114*float64(b)
	nl := lum / 65535
	return nl
}

func AddColorNoise(c color.RGBA, x, y int) color.RGBA {
	fl := math.Sqrt(math.Sqrt(math.Sqrt(Noise(x, y))))
	rs := float64(c.R) / 256 * fl
	gs := float64(c.G) / 256 * fl
	bs := float64(c.B) / 256 * fl
	c.R = uint8(float64(rs) * 255)
	c.G = uint8(float64(gs) * 255)
	c.B = uint8(float64(bs) * 255)
	return c
}

func varyColor(c color.RGBA) color.RGBA {
	g.colorOffsetX += 10
	if g.colorOffsetX >= windowSize {
		g.colorOffsetX = 0
		g.colorOffsetY++
		if g.colorOffsetY >= windowSize {
			g.colorOffsetY = 0
		}
	}
	return AddColorNoise(c, g.colorOffsetX, g.colorOffsetY)
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebiten.Image) {
	// Write your game's rendering.
	screen.Fill(color.RGBA{201, 237, 240, 255})
	for x, row := range g.grains {
		for y, p := range row {
			if p.material.id > 0 {
				var pixelColor color.Color
				pixelColor = AddColorNoise(p.color, x, y)
				if g.showHeat {
					heat := g.heat.checkHeat(x, y)
					if heat > 0.55 {
						pixelColor = colorfulize(pixelColor).BlendLab(colorful.Color{R: 1, G: 0, B: 0}, heat-0.55).Clamped()
					} else if heat < 0.45 {
						pixelColor = colorfulize(pixelColor).BlendLab(colorful.Color{R: 0, G: 0, B: 1}, 0.45-heat).Clamped()
					}
				}
				screen.Set(x, y, pixelColor)
			}
		}
	}
}

// Layout takes the outside size (e.g., the window size) and returns the (logical) screen size.
// If you don't have to adjust the screen size with the outside size, just return a fixed size.
func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return windowSize, windowSize
}

func main() {
	// Specify the window size as you like. Here, a doubled size is specified.
	ebiten.SetWindowSize(windowSize*scaleFactor, windowSize*scaleFactor)
	ebiten.SetWindowTitle("Sand")
	ebiten.SetMaxTPS(60)
	// Call ebiten.RunGame to start your game loop.

	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	file, err := os.Open("./noise.png")
	if err != nil {
		log.Fatal("Error: File could not be opened")
	}
	defer file.Close()
	noise, _, err := image.Decode(file)
	if err != nil {
		fmt.Println("Error: Image could not be decoded")
		os.Exit(1)
	}
	gs := field{}
	hm := heatmap{}
	hm.reset()
	g = &Game{&gs, &hm, noise, 0, 0, 0, false}

	err = ebiten.RunGame(g)
	if err != nil {
		log.Fatal(err)
	}
}
